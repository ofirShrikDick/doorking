class Course {
  String courseName;
  String courseColor;

  Course(String name, String color, {courseName, courseColor}) {
    this.courseName = name;
    this.courseColor = color;
  }

}