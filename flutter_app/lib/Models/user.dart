import 'package:flutter_app/Models/Course.dart';

class MyUser {
  final String userId;
  String userName;
  int doors;
  Course course;

  MyUser({this.userId});

  MyUser.allDetails(this.userId, this.userName, this.doors, this.course);

  void set name(String name) {
    this.userName = name;
  }

  void set doorsOpened(int doorsO) {
    this.doors = doorsO;
  }

  void set courseSet(Course course) {
    this.course = course;
  }
}
