import 'package:flutter/material.dart';
import 'package:flutter_app/Models/user.dart';
import 'package:flutter_app/screens/Home/home.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_app/screens/RegOrLog/LogIn.dart';
import 'package:flutter_app/services/authService.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider.value(
        value: AuthService().userChanges,
        child: MaterialApp(
          home: Wrapper(),
        ));
  }
}

class Wrapper extends StatelessWidget {
  Widget build(BuildContext context) {
    final user = Provider.of<MyUser>(context);
    return user != null ? Home(user) : LogIn();
  }
}
