import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CostumeTextFiled extends StatelessWidget {
  TextEditingController controller;
  String hintText;
  Icon icon;
  Color outlineColor;
  bool isValid;
  String errorMessage;

  CostumeTextFiled(this.controller, this.hintText, this.icon, this.outlineColor,
      {this.isValid = true});

  CostumeTextFiled.forRegister(this.controller, this.hintText, this.icon,
      this.outlineColor, this.isValid, this.errorMessage);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: (TextField(
      controller: controller,
      textAlign: TextAlign.end,
      decoration: InputDecoration(
        suffixIcon: icon,
        hintText: hintText,
        counterText: isValid ? '' : errorMessage,
        counterStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: BorderSide(
            color: outlineColor,
            width: 3.0,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: BorderSide(
            width: 2.0,
          ),
        ),
      ),
    )));
  }
}
