import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NextButton extends StatelessWidget {
  final GestureTapCallback onPressed;

  NextButton(this.onPressed);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: this.onPressed,
      color: Color(0xFF7379F5),
      textColor: Colors.white,
      child: Icon(
        Icons.arrow_back,
        size: 24,
      ),
      padding: EdgeInsets.all(16),
      shape: CircleBorder(),
    );
  }
}
