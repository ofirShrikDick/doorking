import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Models/user.dart';
import 'package:flutter_app/services/authService.dart';
import 'package:flutter_app/services/fireStoreService.dart';

class Home extends StatefulWidget {
  final MyUser user;
  Home(this.user);

  @override
  _HomeState createState() => _HomeState(this.user);
}

class _HomeState extends State<Home> {
  final AuthService _authService = AuthService();
  MyUser user;
  FireStoreService fireStoreService;
  _HomeState(this.user) : fireStoreService = FireStoreService(user.userId);

  @override
  void initState() {
    super.initState();
    initUser();
  }

  void initUser() async {
    MyUser newUser = await fireStoreService.findUserById(this.user);
    setState(() {
      this.user = newUser;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
              Color(0xFF73AEF5),
              Color(0xFF7398F5),
              Color(0xFF7389F5),
              Color(0xFF7379F5),
            ])),
        child: Container(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "התנתק",
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                  ),
                  IconButton(
                      icon: Icon(Icons.logout),
                      onPressed: () async {
                        await _authService.signOut();
                      }),
                ],
              ),
              SizedBox(
                height: 60,
              ),
              Text(
                ',וואלקם ${user.userName}',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
              Text(
                ':כמות הדלתות שפתחת',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                '${user.doors}',
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              MaterialButton(
                onPressed: () async {
                  await fireStoreService.updateDoorsOpened(user.doors++);
                  setState(() {
                    this.user.doorsOpened = user.doors++;
                  });
                },
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.meeting_room,
                      size: 60,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'פתחתי ',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'את הדלת',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                padding: EdgeInsets.all(50),
                shape: CircleBorder(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

  //  Row(
  //               mainAxisAlignment: MainAxisAlignment.end,
  //               children: [
  //                 Text(
  //                   "log out ",
  //                   textAlign: TextAlign.center,
  //                   style:
  //                       TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
  //                 ),
  //                 IconButton(
  //                     icon: Icon(Icons.logout),
  //                     onPressed: () async {
  //                       await _authService.signOut();
  //                     }),
  //               ],
  //             ),