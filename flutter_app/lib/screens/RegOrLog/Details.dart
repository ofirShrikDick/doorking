import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Models/Course.dart';
import 'package:flutter_app/screens/Components/CostumeTextFiled.dart';
import 'package:flutter_app/screens/Components/NextButton.dart';
import 'package:flutter_app/screens/RegOrLog/RegLogLayout.dart';
import 'package:flutter_app/services/CourseService.dart';
import 'package:flutter_app/services/fireStoreService.dart';

class Details extends StatefulWidget {
  final String uid;
  final FireStoreService fireStoreService;

  Details(this.uid) : this.fireStoreService = FireStoreService(uid);

  @override
  _DetailsState createState() => _DetailsState(this.fireStoreService);
}

class _DetailsState extends State<Details> {
  int _value = 1;
  String courseName = 'rzv';
  bool userNameValid = true;
  final userNameConroller = TextEditingController();
  final FireStoreService fireStoreService;

  _DetailsState(this.fireStoreService);

  Future<List<Course>> dropItemsInit() async {
    CourseService courseServe = new CourseService();
    List<Course> courses = await courseServe.courses();

    return courses;
  }

  List<DropdownMenuItem<int>> dropDownItems(List<Course> courses) {
    List<DropdownMenuItem<int>> ddl = <DropdownMenuItem<int>>[];
    courses.forEach((element) {
      ddl.add(
        DropdownMenuItem(
            child: Text(
              element.courseName,
              style: TextStyle(
                color: Colors.black54,
              ),
            ),
            value: courses.indexOf(element) + 1),
      );
    });

    return ddl;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: RegLogLayout(
          Column(
            children: [
              Text(
                'וואלק עוד מידע',
                style: TextStyle(fontSize: 30),
              ),
              SizedBox(
                height: 20,
              ),
              CostumeTextFiled(
                  userNameConroller,
                  'שם משתמש',
                  Icon(
                    Icons.person,
                    color: Colors.black54,
                  ),
                  Colors.black54),
              SizedBox(height: 40),
              Container(
                alignment: Alignment.topRight,
                child: Text(
                  '?קורס',
                  style: TextStyle(fontSize: 25),
                ),
              ),
              Container(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Container(
                    child: DropdownButtonHideUnderline(
                      child: FutureBuilder(
                        future: dropItemsInit(),
                        builder: (
                          BuildContext context,
                          AsyncSnapshot<List<Course>> snapshot,
                        ) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CircularProgressIndicator(),
                                Visibility(
                                  visible: snapshot.hasData,
                                  child: Text(
                                    'חכה קצת מה יש',
                                    style: const TextStyle(
                                        color: Colors.black, fontSize: 10),
                                  ),
                                )
                              ],
                            );
                          } else if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.hasError) {
                              return const Text('Error');
                            } else if (snapshot.hasData) {
                              return DropdownButton(
                                  value: _value,
                                  items: dropDownItems(snapshot.data),
                                  onChanged: (value) {
                                    setState(() {
                                      _value = value;
                                      print(
                                          '${value} is a value ${snapshot.data[value].courseName}');
                                      this.courseName =
                                          snapshot.data[value].courseName;
                                    });
                                  });
                            } else {
                              return const Text('Empty data');
                            }
                          } else {
                            return Text('State: ${snapshot.connectionState}');
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 40),
              Container(
                width: 360,
                child: Row(
                  children: <Widget>[
                    NextButton(() async {
                      setState(() {
                        return this.userNameValid =
                            this.userNameConroller.text.length > 0
                                ? true
                                : false;
                      });

                      if (!this.userNameValid) {
                        print('user couldnt register');
                      } else {
                        CourseService courseServe = new CourseService();
                        String chosenCourse =
                            await courseServe.FindCourseByName(courseName);

                        print('${this.fireStoreService} is the firestore');
                        await this.fireStoreService.updateUserData(
                            userNameConroller.text, 0, chosenCourse);

                        Navigator.pop(context);
                      }
                    })
                  ],
                ),
              ),
              SizedBox(height: 20),
              !userNameValid
                  ? Text(
                      'לא הזנת את כל הפרטים',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.red,
                      ),
                    )
                  : SizedBox(height: 25),
            ],
          ),
        ),
      ),
    );
  }
}
