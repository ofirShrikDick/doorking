import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/screens/Components/CostumeTextFiled.dart';
import 'package:flutter_app/screens/Components/NextButton.dart';
import 'package:flutter_app/screens/RegOrLog/RegLogLayout.dart';
import 'package:flutter_app/screens/RegOrLog/Register.dart';
import 'package:flutter_app/services/authService.dart';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  bool detailsExists = true;
  final emailController = TextEditingController(),
      passwordController = TextEditingController();
  AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return RegLogLayout(
      Column(
        children: [
          Text(
            'סומסום הפתח',
            style: TextStyle(fontSize: 30),
          ),
          SizedBox(
            height: 20,
          ),
          CostumeTextFiled(
              emailController,
              'אימייל',
              Icon(
                Icons.email,
                color: Colors.black54,
              ),
              Colors.black54),
          SizedBox(
            height: 40,
          ),
          CostumeTextFiled(
            passwordController,
            'סיסמה',
            Icon(
              Icons.lock,
              color: Colors.black54,
            ),
            Colors.black54,
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.topRight,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Register()),
                  );
                },
                child: new Text(
                  '!אין משתמש? צור חדש פה ',
                  style: TextStyle(fontSize: 18),
                )),
          ),
          SizedBox(height: 20),
          Container(
            alignment: Alignment.topLeft,
            child: NextButton(
              () async {
                dynamic user = await _auth.signInWithEmailAndPassword(
                    emailController.text, passwordController.text);
                if (user == null) {
                  setState(() {
                    return this.detailsExists = false;
                  });
                  print('wrong user details');
                }
              },
            ),
          ),
          SizedBox(height: 8),
          !detailsExists
              ? Text(
                  'אחד או כמה מהפרטים שהזנת לא נכונים',
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                )
              : SizedBox(height: 20),
        ],
      ),
    );
  }
}
