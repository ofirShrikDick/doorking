import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/screens/Components/CostumeTextFiled.dart';
import 'package:flutter_app/screens/Components/NextButton.dart';
import 'package:flutter_app/screens/RegOrLog/Details.dart';
import 'package:flutter_app/screens/RegOrLog/RegLogLayout.dart';
import 'package:flutter_app/services/authService.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  int _value = 1;
  AuthService _auth = AuthService();
  bool isEmailValid = true,
      passwordValid = true,
      passwordMatches = true,
      emailExist = false;
  final emailController = TextEditingController(),
      passwordController = TextEditingController(),
      passApproveController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  RegExp emailValid = new RegExp(
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$",
      caseSensitive: false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: RegLogLayout(Column(
      children: [
        Container(
          alignment: Alignment.topRight,
          child: Text(
            '?מי פה המלך',
            style: TextStyle(fontSize: 30),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        CostumeTextFiled.forRegister(
            emailController,
            'אימייל',
            Icon(
              Icons.email,
              color: Colors.black54,
            ),
            Colors.black54,
            isEmailValid,
            'כתובת מייל לא תקינה'),
        SizedBox(
          height: 40,
        ),
        CostumeTextFiled.forRegister(
            passwordController,
            'סיסמה',
            Icon(
              Icons.lock,
              color: Colors.black54,
            ),
            Colors.black54,
            passwordValid,
            'הסיסמה חייבת להכל בין 6-14 תווים'),
        SizedBox(
          height: 40,
        ),
        CostumeTextFiled.forRegister(
            passApproveController,
            'אישור סיסמה',
            Icon(
              Icons.lock,
              color: Colors.black54,
            ),
            Colors.black54,
            passwordValid,
            'הסיסמה לא תואמת את הסיסמה שהוזנה'),
        SizedBox(height: 20),
        Row(
          children: [
            NextButton(() async {
              setState(() {
                return this.isEmailValid =
                    EmailValidator.validate(this.emailController.text);
              });
              setState(() {
                var passSize = this.passwordController.text.length;
                return passwordValid = passSize >= 6 && passSize <= 14;
              });
              setState(() {
                return passwordMatches = (this.passwordController.text ==
                        this.passApproveController.text) &&
                    this.passwordValid;
              });

              if (this.isEmailValid &&
                  this.passwordValid &&
                  this.passwordMatches) {
                dynamic user = await _auth.registerWithEmailAndPassword(
                    emailController.text, passwordController.text);
                if (user == null) {
                  setState(() {
                    this.emailExist = true;
                  });

                  print('user couldnt register');
                } else {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Details(user.userId),
                    ),
                  );
                }
              }
            }),
            Spacer(),
            emailExist
                ? Text(
                    'כתובת המייל קיימת במערכת',
                    style: TextStyle(
                        color: Colors.red, fontWeight: FontWeight.bold),
                  )
                : SizedBox(height: 25),
          ],
        ),
      ],
    )));
  }
}
