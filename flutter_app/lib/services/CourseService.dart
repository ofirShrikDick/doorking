import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_app/Models/Course.dart';

class CourseService {
  CourseService() {}

  final CollectionReference courseCollection =
      FirebaseFirestore.instance.collection('Course');

  Future<List<Course>> courses() {
    Future<List<Course>> courses = courseCollection.get().then((value) {
      return value.docs.map((element) {
        return Course(element.data()['course'], element.data()['color']);
      }).toList();
    });

    return courses;
  }

  Future<String> FindCourseByName(String name) async {
    Query query = courseCollection.where('course', isEqualTo: name);
    String id = '0';
    await query.get().then((value) => value.docs.forEach((element) {
          id = element.id;
        }));

    return id;
  }
}
