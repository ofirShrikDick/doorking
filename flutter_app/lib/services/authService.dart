import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_app/Models/user.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  MyUser userFromFireBaseUser(User user) {
    return user != null ? MyUser(userId: user.uid) : null;
  }

  Stream<MyUser> get userChanges {
    print('change');
    return _auth
        .authStateChanges()
        .map((User firebaseUser) => userFromFireBaseUser(firebaseUser));
  }

  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      MyUser user = userFromFireBaseUser(userCredential.user);
      return user;
    } catch (error) {
      print(error);
    }
  }

  Future registerWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);
      MyUser user = userFromFireBaseUser(userCredential.user);
      return user;
    } catch (error) {
      print(error);
    }
  }

  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (error) {
      print(error);
      return null;
    }
  }
}
