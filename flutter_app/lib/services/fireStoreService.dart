import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_app/Models/user.dart';

class FireStoreService {
  String uid;

  FireStoreService(String uid) {
    this.uid = uid;
  }

  final CollectionReference usersCollection =
      FirebaseFirestore.instance.collection('usersDt');

  Future<void> updateUserData(String userName, int doors, String courseDocId) {
    return usersCollection.doc(this.uid).set({
      'userName': userName,
      'doors': doors,
      'course': FirebaseFirestore.instance.doc('Course/${courseDocId}')
    });
  }

  Future<void> updateDoorsOpened(int doors) {
    return usersCollection.doc(this.uid).update({
      "doors": doors,
    });
  }

  Future<MyUser> findUserById(MyUser user) async {
    DocumentReference document = await usersCollection.doc(user.userId);
    Map<String, dynamic> valData =
        await document.get().then((value) => value.data());
    user.doorsOpened = valData['doors'];
    user.name = valData['userName'];

    // fix course part;

    // user.courseSet = valData['course']

    return user;
  }
}
